#!/usr/bin/perl

# Copyright 2012 Liam Crabbe
# All Rights Reserved
# Licensed under the GPLv3

# cdr2iso
# Provides a nicer interface for converting a .cdr file\
# - to an .iso file. Also compares the md5 of the original
# - and converted files to make sure conversion was actually
# - performed.

# Get username for root check

$username = qx(whoami);
chomp $username;

# Do actual root checking

# Uncomment this if block if you want sudo to be required to run this command.
# - Otherwise, leave it commented.

#if ($username ne "root") {
#	print "\a";
#	print "Must be run as root!\n";
#	exit;
#}

# Get arguments for arg amt checking

$num_args = $#ARGV + 1;

# Do actual argument checking

if ($num_args != 2) {
	print "Usage: cdr2iso inputcdr outputiso\n";
	exit;
}

# Set arguments into better vars

$input_cdr=$ARGV[0];
$output_iso=$ARGV[1];
chomp $input_cdr;
chomp $output_iso;

# Check to make sure that $input_cdr does exist and
# - $output_iso doesn't.

unless (-e $input_cdr) {
	print "Error: $input_cdr does not exist!\n";
	exit;
}

if (-e $output_iso) {
	print "Error: $output_iso already exists!\n";
	exit;
}


# Get MD5 of the input to check to see it worked at end

$input_md5 = qx(md5 -q $input_cdr);
chomp $input_md5;

# In later versions check for -v flag and output the md5

# Perform the actual conversion. THIS IS WHERE THE PROGRAM WILL FAIL

system("hdiutil", "makehybrid", "-iso", "-joliet", "-o", "$output_iso", "$input_cdr");

# Do error checking for hdiutil

$hdiutil_exit=$?;
$hdiutil_exit_reason=$!;
chomp $hdiutil_exit;

if ($hdiutil_exit != 0) {
	print "An error has occured.\n";
	print "Command failed: $hdiutil_exit_reasion\n";
	# hdiutil will still make an iso even if fail, so remove.
	system("rm", "$output_iso");
}

# Now get md5 of iso created to compare and make sure its different

$output_md5 = qx(md5 -q $output_iso);
chomp $output_md5;

# Check the md5's

if ($output_md5 eq $input_md5) {
	print "An error has occured and $output_iso is exactly the same as $input_cdr.\n";
	print "Remove $output_iso and try again...\n";
}

# And completion...

print "$input_cdr was successfully converted to $output_iso!\n";


